package ru.ibs.codewars.ssaliy


object BouncingBall {

  def bouncingBall(height: Double, bounce: Double, window: Double): Int = {
    if (height>window && window>0 && bounce>0 && bounce<1) {
      var res: Int=1
      var b: Double=height
      while (b*bounce>window){
        b=b*bounce
        res=res+2
      }
      res
    }
    else -1
  }

  def main(args:Array[String]):Unit={
      if (args.length == 0) {
        println("Default bouncing point: "+bouncingBall(20,0.9,5))
      }
      else {
        val parameters = args.toList
        println("Bouncing point: "+bouncingBall(parameters(0).toDouble, parameters(1).toDouble, parameters(2).toDouble ))
      }
     }
}
