package ru.ibs.codewars.ssaliy

import org.scalatest.Assertions._
import org.scalatest.flatspec.AnyFlatSpec
import ru.ibs.codewars.ssaliy.BouncingBallTest.testing



class BouncingBallTest extends AnyFlatSpec {
  it should "pass basic tests" in {
    testing(3, 0.66, 1.5, 3)
    testing(10, 0.6, 10, -1)
    testing(-5, 0.66, 1.5, -1)
    testing(5, -1, 1.5, -1)

  }
}

object BouncingBallTest {

  private def testing(h: Double, bounce: Double, window: Double, expect: Int): Unit = {
    println("H: " + h + " BOUNCE: " + bounce + " WINDOW: " + window)
    val actual: Int = BouncingBall.bouncingBall(h, bounce, window)
    println("Actual: " + actual)
    println("Expect: " + expect)
    println("-")
    assertResult(expect){actual}
  }
}

