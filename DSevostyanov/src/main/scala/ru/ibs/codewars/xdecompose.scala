object xdecompose {
  def decompose(n: Long): String = {
    var n2: Long = n * n
    var L: List[Long] = List()
    var founded: Boolean = false
    var locSum: BigInt = 0
    var i: Long = n
    var i2: Long = 0

    while (i > 1 && founded == false) {
      i -= 1
      locSum = L.map(x => x * x).sum
      i2 = i * i
      if (locSum + i2 == n2) {
        L = i :: L
        founded = true
      }
      else if (locSum + i2 < n2)
        L = i :: L

      if (i == 1 && founded == false)
        if (!(L.length == 1 && L.head == 1)) {
          if (L.head == 1) L = L.tail
          i = L.head
          L = L.tail
        }
    }
    if (L.head == 1 && L.length == 1) L = L.tail
    if (L.isEmpty) null else L.mkString(" ")
  }


  def main(args: Array[String]): Unit = {
    if (args.length == 1)
      println(decompose(args(0).filter(_.isDigit).toLong))
  }
}
