package ru.ibs.codewars

object Fib extends App {

  type Row = List[BigInt]
  type Matrix = List[Row]

  def multiply(a: Matrix,b: Matrix): Matrix =
    for ( row <- a )
      yield for (col <- b.transpose)
        yield row zip col map Function.tupled(_*_) reduceLeft(_+_)

  def identity(n: Int): Matrix = {
    val m = for (row_idx <- Range(0,n))
      yield for (col_idx <- Range(0,n))
        yield if (row_idx == col_idx) BigInt(1) else BigInt(0)
    m.map(_.toList).toList
  }

  def pow(x:Matrix,n:Int): Matrix = n match {
    case 0 => identity(x(0).length)
    case 1 => x
    case _ => {
      val y = pow(x,(n/2.0).floor.toInt)
      if (n%2==0) multiply(y,y) else multiply(x,multiply(y,y))
    }
  }

  val fibMatrix: Matrix = List(List(1,1),List(1,0))


  def fib(n: Int):BigInt = if (n>=0) pow(fibMatrix,n)(0)(1) else scala.math.pow(-1,-n+1).toInt*pow(fibMatrix,-n)(0)(1)

  println(if(args.length>0) fib(args(0).toInt) else fib(10000))

}
