package ru.ibs.codewars.glazovskiy

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class testRoman extends AnyFlatSpec with Matchers {

  "decode("XXI")" should "return 21" in {

    ru.ibs.codewars.glazovskiy.Roman.decode("XXI") should be(21)

  }

  "decode("XXIV")" should "return 24" in {

    ru.ibs.codewars.glazovskiy.Roman.decode("XXIV") should be(24)

  }

}