package ru.ibs.codewars.glazovskiy

object Roman {

  def decode(roman: String):Int = {
    Map("M" -> 1000, "CM" -> -200, "D" -> 500, "CD" -> -200, "C" -> 100, "XC" -> -20, "L" -> 50, "XL" -> -20, "X" -> 10, "IX" -> -2, "V" -> 5, "IV" -> -2, "I" -> 1)
      .map { case (s, d) => s.r.findAllMatchIn(roman).size * d }
      .sum
  }

  def main(args:Array[String]):Unit={

    var roman="XXI"
    if(args.length==0) roman="XX" else roman=args(0).toString

    println("Число "+roman+" превращается в " + decode(roman)+" .")

  }
}