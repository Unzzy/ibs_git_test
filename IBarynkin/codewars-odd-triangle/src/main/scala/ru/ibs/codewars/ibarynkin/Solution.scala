package ru.ibs.codewars.ibarynkin

object Solution extends App {
  def rowSumOddNumbers(n: Long): Long = n * n * n

  def printHelp: Unit = println(
    s"""
       |Given the triangle of consecutive odd numbers:
       |
       |             1
       |          3     5
       |       7     9    11
       |   13    15    17    19
       |21    23    25    27    29
       |
       |Calculate the sum of the numbers in the nth row of this triangle (starting at index 1) e.g.: (Input --> Output)
       |
       |1 -->  1
       |2 --> 3 + 5 = 8""".stripMargin)

  if (args.size > 0)
    args(0) match {
      case "--help" | "-h" =>
        printHelp
        0
      case _ => if (args(0) forall Character.isDigit) {
        println(Solution.rowSumOddNumbers(args(0).toInt).toString)
        0
      } else {
        println("Wrong arg")
        1
      }
    }
  else printHelp
}
