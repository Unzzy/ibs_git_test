import scala.io.StdIn.readLine
import scala.collection.mutable.ListBuffer

object MyApp{
  def solution(number: Int): Long = {
    var l = new ListBuffer[Long]()
    var cur: Int = 3

    if(number < 3)
      return 0

    while(cur < number) {
      if(cur % 3 == 0)
        l += cur
      else if(cur % 5 == 0)
        l += cur

      cur = cur+1
    }
    println(s"Multiples of 3 and 5: ${l.mkString(", ")}")
    l.sum
  }

  def main(args: Array[String]) {
    val number:Int = readLine("Input a number: ").toInt
    println(s"They sum: ${solution(number)}")
  }

}