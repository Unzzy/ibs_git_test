import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TestMyApp extends AnyFlatSpec with Matchers {

  "solution(0)" should "return 0" in {
    MyApp.solution(0) should be(0)
  }

  "solution(1)" should "return 0" in {
    MyApp.solution(1) should be(0)
  }

  "solution(3)" should "return 3" in {
    MyApp.solution(3) should be(3)
  }

  "solution(5)" should "return 5" in {
    MyApp.solution(5) should be(5)
  }

  "solution(15)" should "return 45" in {
    MyApp.solution(15) should be(45)
  }

  "solution(-21)" should "return 0" in {
    MyApp.solution(-21) should be(0)
  }

}

