package ru.ibs.codewars.akalinin

import java.lang.Math.round

object throwBall {

  def countHigh(v0:Int):Int={

    val v=v0.toFloat/3.6
    val time=v/9.81

    round((time/0.1).toFloat)
    }

  def main(args:Array[String]):Unit={

    var v0=0
    if(args.length==0)v0=0 else v0=args(0).toInt

    println("Highest point - "+countHigh(v0)+" metres")

    }
}
