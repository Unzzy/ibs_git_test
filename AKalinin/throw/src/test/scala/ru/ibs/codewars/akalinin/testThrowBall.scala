package ru.ibs.codewars.akalinin

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class testThrowBall extends AnyFlatSpec with Matchers {

  "countHigh(37)" should "return 10" in {

    ru.ibs.codewars.akalinin.throwBall.countHigh(37) should be(10)

  }

  "countHigh(100)" should "return 28" in {

    ru.ibs.codewars.akalinin.throwBall.countHigh(100) should be(28)

  }
  "countHigh(49)" should "return 14" in {

    ru.ibs.codewars.akalinin.throwBall.countHigh(49) should be(14)

  }
  "countHigh(200)" should "return 57" in {

    ru.ibs.codewars.akalinin.throwBall.countHigh(200) should be(57)

  }

}
